% title           : proj.m
% author          : Martin Cernek
% email           : xcerne01(at)stud.fit.vutbr.cz
% date            : 2014/12/28
% description     : digital image processing

% Nacitanie originalneho obrazu.
orig_img = imread('xcerne01.bmp');
% Otvorenie suboru
reseni = fopen('reseni.txt', 'w');

% ZAOSTRENIE OBRAZU
H = [ -0.5 -0.5 -0.5; -0.5 5.0 -0.5; -0.5 -0.5 -0.5 ];
foc_img = imfilter(orig_img, H);
imwrite(foc_img, 'step1.bmp');

% OTOCENIE OBRAZU
flip_img = fliplr(foc_img);
imwrite(flip_img, 'step2.bmp');

% MEDIANOVY FILTER
med_img = medfilt2(flip_img, [5 5]);
imwrite(med_img, 'step3.bmp');

% ROZMAZANIE OBRAZU
H = [ 1 1 1 1 1; 1 3 3 3 1; 1 3 9 3 1; 1 3 3 3 1; 1 1 1 1 1 ] / 49;
blur_img = imfilter(med_img, H);
imwrite(blur_img, 'step4.bmp');

% CHYBA V OBRAZE
comp_img = fliplr(blur_img);
dcomp_img = double(comp_img);
dorig_img = double(orig_img);
[n,m] = size(orig_img);
noise = 0;
for (ii = 1:n)
	for (jj = 1:m)
		noise = noise + abs(dorig_img(ii, jj) - dcomp_img(ii,jj));
	end;
end;
noise = noise / n / m;
fprintf(reseni,'chyba=%f\n', noise);

% ROZTIAHNUTIE HISTOGRAMU
min_val = double(min(min(blur_img)));
max_val = double(max(max(blur_img)));
hist_img = imadjust(blur_img, [min_val / 255 max_val /255], [0 ; 1]);
imwrite(hist_img, 'step5.bmp');

% SPOCITANIE PRIEMERNEJ HODNOTY A SMERODATNEJ ODCHYLKY
dblur_img = double(blur_img);
dhist_img = double(hist_img);
mean_no_hist = mean2(dblur_img);
fprintf(reseni,'mean_no_hist=%f\n', mean_no_hist);
std_no_hist = std2(dblur_img);
fprintf(reseni,'std_no_hist=%f\n', std_no_hist);
mean_hist = mean2(dhist_img);
fprintf(reseni,'mean_hist=%f\n', mean_hist);
std_hist = std2(dhist_img);
fprintf(reseni,'std_hist=%f\n', std_hist);

% KVANTIZACIA OBRAZU
N = 2;
a = 0;
b = 255;
kvant_img = zeros(n, m);
for (ii = 1:n)
	for (jj = 1:m)
		kvant_img(ii, jj) = round(((2^N)-1)*(double(hist_img(ii, jj))-a)/(b-a))*(b-a)/((2^N)-1) + a;
	end;
end;
imwrite(uint8(kvant_img), 'step6.bmp');